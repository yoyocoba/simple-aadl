package aadl1.dnd;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class Drag {

	private static Tree tree;

	public static void dragPart(Shell shell) {

		tree = new Tree(shell, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL );
		
		TreeItem appSoftwareItem = new TreeItem(tree, 0);
		appSoftwareItem.setText("1. Application Sofware");		
		TreeItem dataItem = new TreeItem(appSoftwareItem, 0);
		dataItem.setText("1. Data");
		TreeItem processItem = new TreeItem(appSoftwareItem, 0);
		processItem.setText("2. Process");
		TreeItem threadGroupItem = new TreeItem(appSoftwareItem, 0);
		threadGroupItem.setText("3. Thread Group");
		TreeItem threadItem = new TreeItem(appSoftwareItem, 0);
		threadItem.setText("4. Thread");
		TreeItem subprogramItem = new TreeItem(appSoftwareItem, 0);
		subprogramItem.setText("5. Subprogram");
		
		TreeItem execPlatformItem = new TreeItem(tree, 0);
		execPlatformItem.setText("2. Execution Platform");		
		TreeItem deviceItem = new TreeItem(execPlatformItem, 0);
		deviceItem.setText("1. Device");
		TreeItem memoryItem = new TreeItem(execPlatformItem, 0);
		memoryItem.setText("2. Memory");
		TreeItem busItem = new TreeItem(execPlatformItem, 0);
		busItem.setText("3. Bus");
		TreeItem processorItem = new TreeItem(execPlatformItem, 0);
		processorItem.setText("4. Processor");
	
		TreeItem compositeItem = new TreeItem(tree, 0);
		compositeItem.setText("3. Composite");
		TreeItem systemItem = new TreeItem(compositeItem, 0);
		systemItem.setText("1. System");	
		
		TreeItem otherItem = new TreeItem(tree, 0);
		otherItem.setText("4. Other");
		TreeItem packageItem = new TreeItem(otherItem, 0);
		packageItem.setText("1. Package");
		
		// TreeItem edgeItem = new TreeItem(tree, 0);
		// edgeItem.setText("Edge");
	}

	public static void dragSource(Transfer[] transfer) {
		DragSource ds = new DragSource(tree, DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK);
		ds.setTransfer(transfer);
		ds.addDragListener(new DragSourceListener() {
			@Override
			public void dragStart(DragSourceEvent event) {
				if (tree.getSelection()[0] == null) {
					event.doit = false;
				}
			}

			@Override
			public void dragSetData(DragSourceEvent event) {
				event.data = tree.getSelection()[0].getText().substring(3);
			}

			@Override
			public void dragFinished(DragSourceEvent event) {
			}
		});
	}
}
