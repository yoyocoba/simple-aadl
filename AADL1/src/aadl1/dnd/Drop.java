package aadl1.dnd;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

import aadl1.shape.*;
import aadl1.util.CanvasItem;
import aadl1.util.CanvasMemory;

public class Drop {

	private static Canvas canvas;
	private static int width = 200;
	private static int height = 100;

	public static void dragPart(Shell shell) {
		canvas = new Canvas(shell, SWT.BORDER);
		canvas.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		canvas.addMenuDetectListener(new MenuDetectListener() {
			@Override
			public void menuDetected(MenuDetectEvent ev) {
				Menu menu = new Menu(shell, SWT.POP_UP);
				if (isItemPoint(shell) != null) {
					MenuItem container = new MenuItem(menu, SWT.NONE);
					container.setText("Open container");
					container.addSelectionListener(new SelectionAdapter() {
						@Override
						public void widgetSelected(SelectionEvent e) {

						}
					});
				}

				menu.setVisible(true);

			}
		});

		canvas.addMouseListener(new MouseListener() {

			Boolean clicked = false;
			int x1;
			int x2;
			int y1;
			int y2;

			@Override
			public void mouseUp(MouseEvent e) {
				x2 = e.x;
				y2 = e.y;
				CanvasItem first = isItemPoint(x1, y1);
				CanvasItem second = isItemPoint(x2, y2);
				if (first != null & second != null) {
					canvas.addPaintListener(new PaintListener() {
						@Override
						public void paintControl(PaintEvent e) {
							e.gc.drawLine(first.getX() + (int) (0.5 * width), first.getY() + height, second.getX() + (int) (0.5 * width), second.getY());
						}
					});
				}
				canvas.redraw();

			}

			@Override
			public void mouseDown(MouseEvent e) {
				x1 = e.x;
				y1 = e.y;
			}

			@Override
			public void mouseDoubleClick(MouseEvent arg0) {

			}
		});
	}

	public static void dropTarget(Transfer[] transfer, Shell shell) {
		DropTarget dt = new DropTarget(canvas, DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_LINK);
		dt.setTransfer(transfer);
		dt.addDropListener(new DropTargetAdapter() {
			public void drop(DropTargetEvent event) {
				String name = (String) event.data;
				addIcon(name, shell);
			}
		});
	}

	public static void addIcon(String name, Shell shell) {

		Point cursorLocation = getLocation(shell);
		int x = cursorLocation.x;
		int y = cursorLocation.y;

		if (canDraw(x, y, width, height)) {

			canvas.addPaintListener(new PaintListener() {
				@Override
				public void paintControl(PaintEvent e) {
					switch (name) {
					case "Data": {
						DataShape.draw(e, x, y, width, height);
						int id = CanvasMemory.getId();
						CanvasItem item = new CanvasItem(x, y, width, height, 11, id);
						CanvasMemory.addItem(item);
						break;
					}
					case "Process": {
						ProcessShape.draw(e, x, y, width, height);
						int id = CanvasMemory.getId();
						CanvasItem item = new CanvasItem(x, y, width, height, 12, id);
						CanvasMemory.addItem(item);
						break;
					}

					case "Thread Group": {
						ThreadGroupShape.draw(e, x, y, width, height);
						int id = CanvasMemory.getId();
						CanvasItem item = new CanvasItem(x, y, width, height, 13, id);
						CanvasMemory.addItem(item);
						break;
					}

					case "Thread": {
						ThreadShape.draw(e, x, y, width, height);
						int id = CanvasMemory.getId();
						CanvasItem item = new CanvasItem(x, y, width, height, 14, id);
						CanvasMemory.addItem(item);
						break;
					}

					case "Subprogram": {
						SubprogramShape.draw(e, x, y, width, height);
						int id = CanvasMemory.getId();
						CanvasItem item = new CanvasItem(x, y, width, height, true, 15, id);
						CanvasMemory.addItem(item);
						break;
					}

					case "Device": {
						DeviceShape.draw(e, x, y, width, height);
						int id = CanvasMemory.getId();
						CanvasItem item = new CanvasItem(x, y, width, height, 21, id);
						CanvasMemory.addItem(item);
						break;
					}

					case "Memory": {
						MemoryShape.draw(e, x, y, width, height);
						int id = CanvasMemory.getId();
						CanvasItem item = new CanvasItem(x, y, width, height, 22, id);
						CanvasMemory.addItem(item);
						break;
					}

					case "Bus": {
						BusShape.draw(e, x, y, width, height);
						int id = CanvasMemory.getId();
						CanvasItem item = new CanvasItem(x, y, width, height, 23, id);
						CanvasMemory.addItem(item);
						break;
					}

					case "Processor": {
						ProcessorShape.draw(e, x, y, width, height);
						int id = CanvasMemory.getId();
						CanvasItem item = new CanvasItem(x, y, width, height, 24, id);
						CanvasMemory.addItem(item);
						break;
					}

					case "System": {
						SystemShape.draw(e, x, y, width, height);
						int id = CanvasMemory.getId();
						CanvasItem item = new CanvasItem(x, y, width, height, true, 31, id);
						CanvasMemory.addItem(item);
						break;
					}

					case "Package": {
						PackageShape.draw(e, x, y, width, height);
						int id = CanvasMemory.getId();
						CanvasItem item = new CanvasItem(x, y, width, height, 41, id);
						CanvasMemory.addItem(item);
						break;
					}

					default:
						break;
					}
				}
			});
		}
		canvas.redraw();
	}

	private static Boolean canDraw(int x, int y, int width, int height) {
		for (CanvasItem item : CanvasMemory.getAllItems()) {
			Boolean lessOX = x + width < item.getX();
			Boolean moreOX = x > item.getX() + item.getWidth();

			Boolean lessOY = y + height < item.getY();
			Boolean moreOY = y > item.getY() + item.getHeight();
			if (!((lessOX | moreOX) | (lessOY | moreOY))) {
				return false;
			}
		}
		return true;
	}

	private static CanvasItem isItemPoint(Shell shell) {
		Point cursorLocation = getLocation(shell);
		for (CanvasItem item : CanvasMemory.getAllItems()) {
			Boolean OX = cursorLocation.x > item.getX() & cursorLocation.x < item.getX() + item.getWidth();
			Boolean OY = cursorLocation.y > item.getY() & cursorLocation.y < item.getY() + item.getHeight();
			if (OX & OY) {
				return item;
			}
		}
		return null;
	}

	private static CanvasItem isItemPoint(int x, int y) {
		for (CanvasItem item : CanvasMemory.getAllItems()) {
			Boolean OX = x > item.getX() & x < item.getX() + item.getWidth();
			Boolean OY = y > item.getY() & y < item.getY() + item.getHeight();
			if (OX & OY) {
				return item;
			}
		}
		return null;
	}

	private static Point getLocation(Shell shell) {
		Point absoluteLocation = shell.getDisplay().getCurrent().getCursorLocation();
		Point relativeLocation = shell.getDisplay().getCurrent().getFocusControl().toControl(absoluteLocation);
		Point canvasLocation = canvas.getLocation();
		int x = relativeLocation.x - canvasLocation.x;
		int y = relativeLocation.y - canvasLocation.y;

		return new Point(x, y);
	}
}
