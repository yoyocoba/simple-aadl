package aadl1.shape;

import org.eclipse.swt.events.PaintEvent;

public class SystemShape {

	public static void draw(PaintEvent e, int x, int y, int width, int height) {
		//Round rectangle
		e.gc.drawRoundRectangle(x, y, width, height, (int) (0.25 * width), (int) (0.25 * height));
	}
}
