package aadl1.shape;

import org.eclipse.swt.events.PaintEvent;

public class MemoryShape {
	
	public static void draw(PaintEvent e, int x, int y, int width, int height) {

		int x1 = x;
		int y1 = y + (int) (0.2 * height);

		int x2 = x + (int) (0.25 * width);
		int y2 = y;

		int x3 = x + (int) (0.75 * width);
		int y3 = y;

		int x4 = x + width;
		int y4 = y + (int) (0.2 * height);

		int x5 = x + width;
		int y5 = y + (int) (0.8 * height);

		int x6 = x + (int) (0.75 * width);
		int y6 = y + height;

		int x7 = x + (int) (0.25 * width);
		int y7 = y + height;

		int x8 = x;
		int y8 = y + (int) (0.8 * height);

		int x9 = x;
		int y9 = y + (int) (0.2 * height);

		int x10 = x + (int) (0.25 * width);
		int y10 = y + (int) (0.4 * height);
		
		int x11 = x + (int) (0.75 * width);
		int y11 = y + (int) (0.4 * height);
		
		int x12 = x + width;
		int y12 = y + (int) (0.2 * height);
		
		int x13 = x + (int) (0.75 * width);
		int y13 = y + (int) (0.4 * height);
		
		int x14 = x + (int) (0.25 * width);
		int y14 = y + (int) (0.4 * height);

		int[] pointArray = new int[] { x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6, x7, y7, x8, y8, x9, y9, x10, y10, x11, y11, x12, y12, x13, y13, x14, y14 };
		e.gc.drawPolygon(pointArray);
	}
}
