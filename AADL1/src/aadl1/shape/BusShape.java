package aadl1.shape;

import org.eclipse.swt.events.PaintEvent;

public class BusShape {

	public static void draw(PaintEvent e, int x, int y, int width, int height) {

		int x1 = x + (int) (0.2 * width);
		int y1 = y;

		int x2 = x + (int) (0.2 * width);
		int y2 = y + (int) (0.25 * height);

		int x3 = x + (int) (0.8 * width);
		int y3 = y + (int) (0.25 * height);

		int x4 = x + (int) (0.8 * width);
		int y4 = y;

		int x5 = x + width;
		int y5 = y + (int) (0.5 * height);

		int x6 = x + (int) (0.8 * width);
		int y6 = y + height;

		int x7 = x + (int) (0.8 * width);
		int y7 = y + (int) (0.75 * height);

		int x8 = x + (int) (0.2 * width);
		int y8 = y + (int) (0.75 * height);

		int x9 = x + (int) (0.2 * width);
		int y9 = y + height;

		int x10 = x;
		int y10 = y + (int) (0.5 * height);

		int[] pointArray = new int[] { x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6, x7, y7, x8, y8, x9, y9, x10, y10 };
		e.gc.drawPolygon(pointArray);
	}
}
