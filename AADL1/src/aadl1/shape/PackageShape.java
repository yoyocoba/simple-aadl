package aadl1.shape;

import org.eclipse.swt.events.PaintEvent;

public class PackageShape {
	
	public static void draw(PaintEvent e, int x, int y, int width, int height) {

		int x1 = x;
		int y1 = y + (int) (0.2 * height);

		int x2 = x + (int) (0.1 * width);
		int y2 = y;

		int x3 = x + (int) (0.3 * width);
		int y3 = y;

		int x4 = x + (int) (0.4 * width);
		int y4 = y + (int) (0.2 * height);

		int x5 = x + width;
		int y5 = y + (int) (0.2 * height);

		int x6 = x + width;
		int y6 = y + height;

		int x7 = x;
		int y7 = y + height;


		int[] pointArray = new int[] { x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6, x7, y7 };
		e.gc.drawPolygon(pointArray);
	}
}
