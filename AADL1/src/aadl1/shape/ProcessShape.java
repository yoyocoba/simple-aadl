package aadl1.shape;

import org.eclipse.swt.events.PaintEvent;

public class ProcessShape {
	
	public static void draw(PaintEvent e, int x, int y, int width, int height) {

		int x1 = x + (int) (0.25 * width);
		int y1 = y;

		int x2 = x + width;
		int y2 = y;

		int x3 = x + (int) (0.75 * width);
		int y3 = y + height;

		int x4 = x;
		int y4 = y + height;
		
		int[] pointArray = new int[] { x1, y1, x2, y2, x3, y3, x4, y4 };
		e.gc.drawPolygon(pointArray);
	}
}
