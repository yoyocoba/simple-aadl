package aadl1.shape;

import org.eclipse.swt.events.PaintEvent;

public class SubprogramShape {
	
	public static void draw(PaintEvent e, int x, int y, int width, int height) {
		//ellipse
		e.gc.drawOval(x, y, width, height);
	}
}