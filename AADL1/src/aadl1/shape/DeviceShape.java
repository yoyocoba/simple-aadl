package aadl1.shape;

import org.eclipse.swt.events.PaintEvent;

public class DeviceShape {

	public static void draw(PaintEvent e, int x, int y, int width, int height) {

		int x1 = x;
		int y1 = y;

		int x2 = x + width;
		int y2 = y;

		int x3 = x + width;
		int y3 = y + height;

		int x4 = x;
		int y4 = y + height;
		
		int x5 = x;
		int y5 = y;

		int x6 = x + (int) (0.1 * width);
		int y6 = y + (int) (0.1 * height);

		int x7 = x + (int) (0.9 * width);
		int y7 = y + (int) (0.1 * height);

		int x8 = x + width;
		int y8 = y;

		int x9 = x + (int) (0.9 * width);
		int y9 = y + (int) (0.1 * height);

		int x10 = x + (int) (0.9 * width);
		int y10 = y + (int) (0.9 * height);

		int x11 = x + width;
		int y11 = y + height;
		
		int x12 = x + (int) (0.9 * width);
		int y12 = y + (int) (0.9 * height);
		
		int x13 = x + (int) (0.1 * width);
		int y13 = y + (int) (0.9 * height);
		
		int x14 = x;
		int y14 = y + height;
		
		int x15 = x + (int) (0.1 * width);
		int y15 = y + (int) (0.9 * height);
		
		int x16 = x + (int) (0.1 * width);
		int y16 = y + (int) (0.1 * height);

		int[] pointArray = new int[] { x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6, x7, y7, x8, y8, x9, y9, x10, y10, x11, y11, x12, y12, x13, y13, x14, y14, x15, y15, x16, y16 };
		e.gc.drawPolygon(pointArray);
	}
}
