package aadl1.shape;

import org.eclipse.swt.events.PaintEvent;

public class ProcessorShape {

	public static void draw(PaintEvent e, int x, int y, int width, int height) {

		int x1 = x;
		int y1 = y + (int) (0.25 * height);
		
		int x2 = x + (int) (0.25 * width);
		int y2 = y;

		int x3 = x + width;
		int y3 = y;

		int x4 = x + width;
		int y4 = y + (int) (0.75 * height);

		int x5 = x + (int) (0.75 * width);
		int y5 = y + height;

		int x6 = x;
		int y6 = y + height;

		int x7 = x;
		int y7 = y + (int) (0.25 * height);

		int x8 = x + (int) (0.75 * width);
		int y8 = y + (int) (0.25 * height);

		int x9 = x + (int) (0.75 * width);
		int y9 = y + height;

		int x10 = x + (int) (0.75 * width);
		int y10 = y + (int) (0.25 * height);

		int x11 = x + width;
		int y11 = y;
		
		int x12 = x + (int) (0.75 * width);
		int y12 = y + (int) (0.25 * height);

		int[] pointArray = new int[] { x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6, x7, y7, x8, y8, x9, y9, x10, y10, x11, y11, x12, y12 };
		e.gc.drawPolygon(pointArray);
	}
}
