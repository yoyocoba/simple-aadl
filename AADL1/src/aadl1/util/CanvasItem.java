package aadl1.util;

public class CanvasItem {

	private int id;
	private int x;
	private int y;
	private int width;
	private int height;
	private int type;
	private Boolean isRounded;
	

	public CanvasItem(int x, int y, int width, int height, Boolean isRounded, int type, int id) {
		setX(x);
		setY(y);
		setWidth(width);
		setHeight(height);		
		setRounded(isRounded);
		setType(type);
		setId(id);
	}

	public CanvasItem(int x, int y, int width, int height, int type, int id) {
		setX(x);
		setY(y);
		setWidth(width);
		setHeight(height);		
		setRounded(false);
		setType(type);
		setId(id);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Boolean isRounded() {
		return isRounded;
	}

	public void setRounded(Boolean isRounded) {
		this.isRounded = isRounded;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
