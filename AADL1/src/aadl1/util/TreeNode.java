package aadl1.util;

import java.util.ArrayList;
import java.util.List;

public class TreeNode {

	private List<String> data;
	private TreeNode parent;
	List<TreeNode> children;

	public TreeNode(List<String> newData) {
		data = newData;
		this.children = new ArrayList<TreeNode>();
	}

	public TreeNode addChild(List<String> child) {
		TreeNode childNode = new TreeNode(child);
		childNode.parent = this;
		this.children.add(childNode);
		return childNode;
	}
	
	public void addItemtoNode(String newItem) {
		this.data.add(newItem);
	}

	public TreeNode getParent() {
		return parent;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void printTree() {
		System.out.println(data);
		for (TreeNode node : children) {
			node.printTree();
		}
	}
}