package aadl1.util;

import java.util.ArrayList;
import java.util.List;

public class CanvasMemory {

	private static List<CanvasItem> memory = new ArrayList<CanvasItem>();
	private static int counter = 0;

	public static CanvasItem getItem(int id) {
		for (CanvasItem item : memory) {
			if(item.getId() == id) {
				return item;
			}
		}
		return null;
	}
	
	public static List<CanvasItem> getAllItems() {
		return memory;
	}
	
	public static Boolean changeItem(CanvasItem newItem) {
		for (CanvasItem item : memory) {
			if(item.getId() == newItem.getId()) {
				item = newItem;
				return true;
			}
		}
		return false;
	}
	
	public static void addItem(CanvasItem item) {
		counter++;
		memory.add(item);
	}

	public static void addItems(ArrayList<CanvasItem> items) {
		items.forEach(i -> addItem(i));
	}
	
	public static void deleteItem(CanvasItem item) {		
		memory.remove(item);
	}

	public static void deleteItems(ArrayList<CanvasItem> items) {
		items.forEach(i -> deleteItem(i));
	}
	
	public static void deleteALLItems(ArrayList<CanvasItem> items) {
		memory.clear();
	}
	
	public static int getId() {
		return counter++;
	}
}
