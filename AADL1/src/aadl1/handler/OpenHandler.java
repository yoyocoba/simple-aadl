package aadl1.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import aadl1.dnd.Drag;
import aadl1.dnd.Drop;

public class OpenHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent ev) throws ExecutionException {
		Shell parentShell = HandlerUtil.getActiveShellChecked(ev);
		Shell shell = new Shell(parentShell.getDisplay(), SWT.SHELL_TRIM | SWT.CENTER);
		shell.setLayout(new FillLayout());		
		
		Transfer[] transfer = new Transfer[] { TextTransfer.getInstance() };
		Drag.dragPart(shell);
		Drop.dragPart(shell);
		
		Drag.dragSource(transfer);
		Drop.dropTarget(transfer, shell);
		
		shell.setSize(1000, 1000);
		shell.open();			
		return null;
	}
	
}
